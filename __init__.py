# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import stock
from . import purchase
from . import sale


def register():
    Pool.register(
        party.Address,
        party.AddressLocation,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        stock.ShipmentIn,
        stock.ShipmentInReturn,
        module='stock_party_address_location', type_='model')
    Pool.register(
        purchase.Purchase,
        purchase.PurchaseLine,
        module='stock_party_address_location', type_='model',
        depends=['purchase'])
    Pool.register(
        sale.SaleLine,
        module='stock_party_address_location', type_='model',
        depends=['sale'])
